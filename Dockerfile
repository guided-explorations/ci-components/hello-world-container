FROM alpine
ADD templates/hello.sh /
ENTRYPOINT ["/bin/sh", "/hello.sh"]