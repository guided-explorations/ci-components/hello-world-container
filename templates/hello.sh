#!/bin/sh
cat <<EOF
Hello from version: _UPDATE_DURING_COMPONENT_PUBLISH_ This container just shows how the CI Component Publishing Utilities can build and manage a component that has a companion container.
It keeps the version of the container and the component the same.
EOF